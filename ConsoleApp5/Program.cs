﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Ninject.IKernel kernel = new StandardKernel();
            kernel.Bind<IConvert>().To<Convert>();
            var con = kernel.Get<IConvert>();
            con.ConvertRUR(7);
            Console.Read();
        }
    }
}
