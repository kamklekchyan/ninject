﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class ConvertModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IConvert>().To<Convert>();
        }
    }
}
