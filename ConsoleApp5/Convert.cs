﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Convert : IConvert
    {
        public void ConvertEUR(int amd)
        {
            Console.WriteLine(amd*576);
        }

        public void ConvertRUR(int amd)
        {
            Console.WriteLine(amd*7.8);
        }

        public void ConvertUSD(int amd)
        {
            Console.WriteLine(amd*476);
        }
    }
}
