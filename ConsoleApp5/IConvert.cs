﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    interface IConvert
    {
        void ConvertUSD(int amd);
        void ConvertEUR(int amd);
        void ConvertRUR(int amd);
    }
}
