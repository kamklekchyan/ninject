﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Table
    {
        private IConvert _convert;
        public Table(IConvert service)
        {
            _convert = service;
        }

        public void ConvertUSD(int amd) => _convert.ConvertUSD(amd);
        public void ConvertEUR(int amd) => _convert.ConvertEUR(amd);
        public void ConvertRUR(int amd) => _convert.ConvertRUR(amd);
    }
}
